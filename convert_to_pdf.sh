#!/usr/bin/env bash

script="$(readlink -f "${BASH_SOURCE[0]}")"
cdir="$(dirname "$script")"

if [ -z $1 ]; then 
	export OUTDIR=$cdir/output
	echo "Используется папка по умолчанию. Запустите $0 --help для справки."
else
	if [ $1 == "--help" ]; then 
		echo """Если запустить скрипт без параметров, то будет использоваться папка по пути:
$cdir/output

Если вы хотите использовать другую папку с таблицами, укажите её имя в параметрах, например:
$0 another_directory

Если вы хотите использовать абсолютный путь, укажите флаг --absolute перед именем папки, например:
$0 --absolute \"$HOME/Documents/Yet another folder with excel tables in it\"

Для работы скрипта должен быть установлен пакет libreoffice""";
		exit 1;
	elif [ $1 == "--absolute" ]; then 
		export OUTDIR=$2;
	else
		export OUTDIR=$cdir/$1;
	fi
fi

command -v libreoffice >/dev/null 2>&1 || { echo >&2 "LibreOffice не найден"; exit 1; }

echo "Используется папка $OUTDIR"

libreoffice --convert-to pdf $OUTDIR/*.xlsx --outdir $OUTDIR

echo "Конвертация завершена"
