import datetime
import os

from mako.template import Template
from openpyxl import Workbook
from openpyxl.styles import Font, PatternFill, Alignment
from openpyxl.styles.borders import Border, Side
from openpyxl.utils import get_column_letter

from settings import *

date_ft = Font()
date_ft.size = DATE_FONT_SIZE
name_ft = Font()
name_ft.size = NAME_FONT_SIZE
hint_ft = Font()
hint_ft.size = HINT_FONT_SIZE
title_ft = Font()
title_ft.size = TITLE_FONT_SIZE
title_ft.bold = True

thin_border = Border(left=Side(style='thin'),
                     right=Side(style='thin'),
                     top=Side(style='thin'),
                     bottom=Side(style='thin'))

alignment_center = Alignment(horizontal='center', vertical='center',
                             shrink_to_fit=True)
alignment_middle_left = Alignment(horizontal='left', vertical='center',
                                  shrink_to_fit=True)
alignment_top_left = Alignment(horizontal='left', vertical='top',
                               shrink_to_fit=True)


def get_leap_day(month, year):
    if (month == 2) and (
            (year % 400 == 0) or ((year % 4 == 0) and (year % 100 != 0))):
        return 1
    return 0


def set_column_appearance(ws, column, n_rows, column_width, column_font,
                          column_alignment):
    ws.column_dimensions[column].width = column_width
    for j in range(n_rows):
        cell = column + str(j + LIST_MARGIN_TOP)
        ws[cell].font = column_font
        ws[cell].border = thin_border
        ws[cell].alignment = column_alignment


def generate_table(output_file_name, table_title, grade, grade_letter, month,
                   year):
    wb = Workbook()
    ws = wb.active
    ws.set_printer_settings(orientation=SHEET_ORIENTATION,
                            paper_size=SHEET_PAPER_SIZE)

    days_in_month = int(MONTH_DAYS[month]) + get_leap_day(int(month), year)

    # Fill the top row with days of the month highlighting the weekends
    for i in range(days_in_month):
        column = get_column_letter(i + LIST_MARGIN_LEFT)
        cell = column + str(LIST_MARGIN_TOP)
        weekday_number = datetime.datetime(year, int(month), i + 1).weekday()
        weekday = WEEK_DAYS[weekday_number]
        ws[cell] = str(i + 1).zfill(2) + "\n" + weekday

        if weekday_number in MARKED_DAYS:
            ws[cell].fill = PatternFill(start_color=MARKED_DAYS_COLOR,
                                        fill_type="solid")
        set_column_appearance(ws, column,
                              len(PUPIL_LIST[grade][grade_letter]) + 1,
                              DATE_COLUMN_WIDTH, date_ft,
                              alignment_center)

    # Fill the left column with the names of the pupils
    for i in range(0, len(PUPIL_LIST[grade][grade_letter])):
        cell = 'A' + str(i + LIST_MARGIN_TOP + 1)
        pupil_name = PUPIL_LIST[grade][grade_letter][i].split()
        pupil_last_name, pupil_first_name = pupil_name[:2]
        ws[cell] = pupil_last_name + " " + pupil_first_name
        set_column_appearance(ws, 'A', len(PUPIL_LIST[grade][grade_letter]) + 1,
                              NAME_COLUMN_WIDTH, name_ft,
                              alignment_middle_left)
        if CELL_CONTENT:
            for j in range(days_in_month):
                column = get_column_letter(j + LIST_MARGIN_LEFT)
                cell = column + str(i + LIST_MARGIN_TOP + 1)
                weekday_number = datetime.datetime(year, int(month),
                                                   j + 1).weekday()
                ws[cell] = pupil_last_name[0] + pupil_first_name[
                    0] + '\n' + str(
                    j + 1).zfill(2)
                ws[cell].alignment = alignment_top_left
                ws[cell].font = hint_ft

                if weekday_number in MARKED_DAYS:
                    ws[cell].fill = PatternFill(
                        start_color=MARKED_DAYS_COLUMN_COLOR,
                        fill_type="solid")

    # Add table title to the corner cell
    title_cell = 'A' + str(LIST_MARGIN_TOP)
    ws[title_cell] = table_title
    ws[title_cell].font = title_ft
    ws[title_cell].alignment = alignment_center

    # Adjust row height
    for i in range(LIST_MARGIN_TOP,
                   len(PUPIL_LIST[grade][grade_letter]) + LIST_MARGIN_TOP + 1):
        ws.row_dimensions[i].height = ROW_HEIGHT

    # Adjust the margins for printing
    ws.page_margins.left = SHEET_MARGIN_LEFT
    ws.page_margins.right = SHEET_MARGIN_RIGHT

    # Finally, save the workbook to the file
    wb.save(filename=output_file_name)


if __name__ == "__main__":
    if not os.path.exists(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)

    table_filename_template = Template(TABLE_FILENAME)
    table_title_template = Template(TABLE_TITLE)


    def gen(month_range, year):
        for month in month_range:
            for grade in PUPIL_LIST.keys():
                for grade_letter in PUPIL_LIST[grade].keys():
                    str_month = str(month).zfill(2)

                    template_variables = {**vars(), **globals()}
                    template_variables.update(dict(MONTH_NAMES=MONTH_NAMES,
                                                   MONTH_DAYS=MONTH_DAYS,
                                                   WEEK_DAYS=WEEK_DAYS,
                                                   PUPIL_LIST=PUPIL_LIST))

                    table_title = table_title_template.render(
                        **template_variables)
                    template_variables.update(table_title=table_title)
                    table_filename = table_filename_template.render(
                        **template_variables)

                    generate_table(table_filename,
                                   table_title, grade, grade_letter, str_month,
                                   year)


    year = YEAR
    if FIRST_SCHOOL_MONTH > LAST_SCHOOL_MONTH:
        month_range = range(FIRST_SCHOOL_MONTH, 12 + 1)
        gen(month_range, year)
        year += 1
        month_range = range(1, LAST_SCHOOL_MONTH + 1)
    else:
        month_range = range(FIRST_SCHOOL_MONTH, LAST_SCHOOL_MONTH + 1)
    gen(month_range, year)
