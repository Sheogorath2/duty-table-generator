import json
import sys

# List of pupils is loaded from json file. If you prefer you can define it here.
PUPIL_LIST_FILENAME = sys.argv[1]
PUPIL_LIST = json.loads(open(PUPIL_LIST_FILENAME, 'r').read())

MONTH_NAMES = {"01": "Январь", "02": "Февраль", "03": "Март", "04": "Апрель",
               "05": "Май", "06": "Июнь", "07": "Июль",
               "08": "Август", "09": "Сентябрь", "10": "Октябрь",
               "11": "Ноябрь", "12": "Декабрь"}
MONTH_DAYS = {"01": 31, "02": 28, "03": 31, "04": 30, "05": 31, "06": 30,
              "07": 31, "08": 31, "09": 30, "10": 31,
              "11": 30, "12": 31}
WEEK_DAYS = ["ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ", "ВС"]

TABLE_TITLE = "Дежурные ${grade}${grade_letter}. ${MONTH_NAMES[str_month]} ${year}"
TABLE_FILENAME = "output/${table_title}.xlsx"

DATE_FONT_SIZE = 6
DATE_COLUMN_WIDTH = 2

NAME_FONT_SIZE = 10
NAME_COLUMN_WIDTH = 19.5

HINT_FONT_SIZE = 3

TITLE_FONT_SIZE = 8

ROW_HEIGHT = 25

MARKED_DAYS = [5, 6]
MARKED_DAYS_COLOR = "000000"
MARKED_DAYS_COLUMN_COLOR = "dddddddd"

CELL_CONTENT = True

YEAR = 2017
FIRST_SCHOOL_MONTH = 9
LAST_SCHOOL_MONTH = 5

SHEET_MARGIN_LEFT = 0.25
SHEET_MARGIN_RIGHT = 0.25

SHEET_ORIENTATION = 'portrait'
SHEET_PAPER_SIZE = 9  # set 9 for A4

# table offset in cells
LIST_MARGIN_TOP = 2
LIST_MARGIN_LEFT = 2

OUTPUT_DIR = "output"
